---
title: "Study Area Demographics"
draft: false
weight: 30
---
Careful analysis of the mobility needs of various subsets of the population, and potential ridership of transit services based on certain quantifiable factors, is key in developing and evaluating transit plans. As part of the development process of the Region 8 HSTP, the U.S. Census Bureau's American Community Survey (ACS) data was collected to identify and illustrate the distribution of the following populations considered by the transportation and human service sectors as more likely to be dependent on transit services:

- Youth (0 to 17 years)
- Older adults (60+)
- Persons with disabilities
- Persons and families with low-income
- Zero-vehicle households
- Veterans

In general, the characteristics of these groups increase the likelihood that the individuals do not drive, for a variety of reasons, making carpooling and transit the only viable alternatives available. This section also includes demographic information on racial and ethnic minorities in the region in order to ensure this plan, and any outreach efforts based on this plan, includes constituencies that are traditionally underserved or could warrant distinct outreach efforts or language assistance.

The four types of limitations which preclude persons from driving are: physical (a disability or health condition), financial (insufficient funds to purchase or maintain a personal vehicle), legal (suspended or revoked license, or children who are not old enough to drive), and self-imposed (a personal choice to refrain from driving some or all of the time for any reason other than those listed). U.S. Census data is generally capable of providing information about the first three categories of limitation, while the fourth is currently recognized as representing a small but significant proportion of transit ridership. The most recent demographic data comes from the 2018 ACS administered by the United States Census Bureau, and is available at the block group level for most demographics. There are several factors which affect demand, not all of which can be projected, however demand estimation is an important task in the development of any transportation transit plan. The populations data in this plan were collected at the Block Group level, unless otherwise indicated, to provide the most detailed spatial analysis available for each target population.

It is important to note for all tables, maps, and figures in this plan, population data for Champaign, Macon, and Vermilion Counties exclude the urbanized areas of Champaign-Urbana, Decatur, and Danville, respectively; only rural populations are represented. The urbanized areas are however taken into consideration when assessing trip generators and employers.

County seats are plotted on the maps in this section for spatial reference, however the addition of labels for each seat detracted from the visibility of the information depicted. For reference, the county seats are: Urbana (Champaign County), Marshall (Clark), Charleston (Coles), Toledo (Cumberland), Clinton (DeWitt), Tuscola (Douglas), Paris (Edgar), Decatur (Macon), Sullivan (Moultrie), Monticello (Piatt), Shelbyville (Shelby), and Danville (Vermilion).

### Age

For the purposes of the HSTP, older adults are defined as 60 years or older, and individuals 17 and under are considered youth. Section 5310 funding specifically aims to expand public mobility options for older adults as deteriorating health and other factors limit the feasibility of personal vehicle use. For older adults who are no longer able, or prefer not to drive, critical services may be inaccessible without general public or specialized transportation. Youth are limited in terms of mobility until they obtain a driver’s license.

<iframe src="https://maps.ccrpc.org/agemap/" width="100%" height="600" allowfullscreen="true"></iframe>

<rpc-table url="Age_2019.csv"
  table-title="Youth, Adult, and Older Adult Populations "
  source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B01001"
  source-url="https://data.census.gov/cedsci/table?q=B01001&g=0100000US_0400000US17_0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183_400C100US15211,22204,22717&tid=ACSDT5Y2019.B01001"
  text-alignment="l,r"></rpc-table>


Region 8 has a population of 73,822 youth, making up 21.6 percent of the total population. This percentage is slightly lower than the state (22.83 percent) and national (22.77 percent) levels. With a population of 177,050, adults age 18 to 59 account for 56.59 percent of the total Region 8 population. This comes in lower than the state and national levels, 55.89 percent and 56.90 percent respectively. Older adults account for 22.97 percent of the regional population (79,344). Older adults make up 20.87 percent of the population in Illinois, and 21.34 percent of the United States population.

### Population with Disabilities

According to the U.S. Census Bureau, the American Community Survey (ACS) covers six disability types: hearing difficulty, vision difficulty, cognitive difficulty, ambulatory difficulty, self-care difficulty, and independent living difficulty. These categories account for many of the reasons that individuals may require public or specialized transportation services.

Region 8 has 37,834 individuals living with at least one disability, making up 12.59 percent of the total population. This percentage is higher than that of the state (11.08 percent), and relates closely to the national proportion (12.61 percent). In Region 8, 4.21 percent of youth (2,749 individuals), 11.02 percent of adults age 18 to 64 (18,087), and 24.03 percent of older adults 65 and over (17,012) live with a disability. Disability status is not available in the same age groupings as other 2018 ACS data, thus the older adult population for disability refers to individuals 65 and over, rather than 60 and over which is how the HSTP defines older adults for all other purposes.


<iframe src="https://maps.ccrpc.org/disabilitiesmap/" width="100%" height="600" allowfullscreen="true"></iframe>

<rpc-chart url="Disability_2019_chart.csv"
  chart-title="Percentage of Individuals with a Disability by Age"
  x-label="Percentage of Individuals with a Disability"
  y-label="County"
  type="horizontalBar"
  stacked="true"
  legend-position="right"
  aspect-ratio="3"
  source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B01001 and Table B18101"
  source-url="https://data.census.gov/cedsci/table?q=B18101&g=0100000US_0400000US17_0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183_400C100US15211,22204,22717&tid=ACSDT5Y2019.B18101"></rpc-chart>

Cumberland, Clark, and Edgar Counties have the largest percentage of total individuals living with a disability. Coles County has a significant population of youth living with a disability (5.9 percent), while Douglas and Shelby Counties have the lowest percentage of youth living with a disability (3.11 percent and 3.32 percent, respectively). Clark County, at 12.89 percent, has the highest percentage of adults age 18 to 64 living with a disability, and Champaign County has the lowest percentage of individuals with a disability in this age group (8.54 percent). Clark County also has the largest proportion of older adults 65 years and older living with a disability (41.83 percent), while Vermillion County has the smallest proportion (27.28 percent) of older adults living with a disability.

<rpc-table url="Disability_2019.csv"
  table-title="Population with Disabilities"
  source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B01001 and Table B18101"
  source-url="https://data.census.gov/cedsci/table?q=B18101&g=0100000US_0400000US17_0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183_400C100US15211,22204,22717&tid=ACSDT5Y2019.B18101"
  text-alignment="l,r"></rpc-table>


### Low-Income Population

The American Community Survey (ACS) determines poverty status by comparing a householder's total family income to established poverty thresholds for the twelve months prior to questionnaire date. Family size, number of related children under the age of eighteen, and for one and two-person householders, the age of the householder are all input variables that influence the poverty threshold. The poverty determination is independent of regional cost of living variances. Household income and size factors are also not affected by the income or presence of any unrelated individuals in the residence. If a household is considered below the poverty level, then the householder and all related family members are counted as living below poverty.

The total population living below poverty is calculated by the sum of all related people from families living in poverty and the number of all unrelated individuals whose individual incomes independently met the poverty threshold. Financial constraints factor into transit-dependency due to the cost associated with the purchase and maintenance of a reliable personal vehicle, among other reasons.

<iframe src="https://maps.ccrpc.org/povertymap/" width="100%" height="600" allowfullscreen="true"></iframe>

<rpc-chart url="Poverty_2019_chart.csv"
  chart-title="Percentage of Low-Income Individuals by Age"
  x-label="Percentage of Low-Income Individuals"
  y-label="County"
  type="horizontalBar"
  stacked="true"
  legend-position="right"
  aspect-ratio="3"
  source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B01001 and Table B17001"
  source-url="https://data.census.gov/cedsci/table?q=B17001&g=0100000US_0400000US17_0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183_400C100US15211,22204,22717&tid=ACSDT5Y2019.B17001"></rpc-chart>


Region 8 has 34,237 individuals living below poverty, making up 11.39 percent of the total population. This percentage is slightly lower than both the state and national levels, 12.1 percent and 13.1 percent respectively. In Region 8, 15.43 percent of youth (10,115 individuals), 12.7 percent of adults age 18 to 64 (22,838), and 4.63 percent of older adults 65 and over (3,284) live below poverty. Poverty status is not available in the same age groupings as other 2018 ACS data, thus the older adult population for poverty refers to individuals 65 and over, rather than 60 and over which is how the HSTP defines older adults for all other purposes.

Edgar, Coles, and Cumberland Counties have the largest percentages of total individuals living below poverty, while Piatt and Macon Counties have the smallest percentages. Coles County has the largest proportion of youth living below poverty (25.38 percent), while Piatt County has the lowest percentage (5.17 percent). Coles County, at 20.2 percent, also has the highest percentage of adults age 18 to 64 living below poverty, and Macon County has the lowest percentage of individuals living below poverty in this age group (5.12 percent). Douglas County has the largest proportion of older adults 65 years and older living below poverty (9.55 percent), while Piatt County has the smallest proportion (2.9 percent) of older adults living below poverty.

<rpc-table url="Poverty_2019.csv"
  table-title="Population Living Below Poverty Level"
  source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B01001 and Table B17001"
  source-url="https://data.census.gov/cedsci/table?q=B17001&g=0100000US_0400000US17_0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183_400C100US15211,22204,22717&tid=ACSDT5Y2019.B17001"
  text-alignment="l,r"></rpc-table>

Poverty data is not currently available at the block group level for individuals, however block group data is available for households. Map 5 illustrates the number of households below the poverty level by census block group within Region 8. In nearly half of the Region 8 counties, the largest number of households below the poverty level are located adjacent to the county seats: Marshall (Clark County), Toledo (Cumberland County), Clinton (DeWitt County), Paris (Edgar County), and Monticello (Piatt County). Douglas, Macon, Moultrie, and Shelby counties have smaller distributions of their highest number of households below the poverty level. Coles County loosely follows the trends. The county seat is located in Charleston, the interstate passes closest to Mattoon, and the highest distributions are found in the block groups situated between both cities. Champaign County continues to follow its trend by having the largest distributions in communities along the interstates. Vermilion County also continues to follow its trend with its largest number of households below the poverty level being located around Hoopeston.

### Zero-Vehicle Households

Outside of carpooling and utilization of taxis, households lacking access to a personal vehicle are heavily reliant on transit. According to the ACS, 6,649 of the 116,092 households in Region 8 (5.72 percent) do not own a vehicle, detailed by county in the table below. Region 8 has a much lower proportion of zero-vehicle households than Illinois and the United States, 10.82 percent and 8.7 percent respectively.

<iframe src="https://maps.ccrpc.org/zerovehicles/" width="100%" height="600" allowfullscreen="true"></iframe>

<rpc-table url="ZeroVeh_2019.csv"
  table-title="Zero Vehicle Households"
  source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B08201"
  source-url="https://data.census.gov/cedsci/table?q=B08201&g=0100000US_0400000US17_0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183_400C100US15211,22204,22717&tid=ACSDT5Y2019.B08201"
  text-alignment="l,r"></rpc-table>

Douglas and Moultrie Counties have the highest proportion of households without access to a vehicle, while Clark, Macon, and Piatt Counties have very low rates for this demographic.

One limitation of this data is that it does not account for households in which multiple adults share a vehicle. Individuals in that scenario are also more likely to rely on transportation services when carpooling is not feasible. In addition, reliability of the personal vehicle is not considered, meaning that the number of households without access to a reliable vehicle may be higher than reflected in these numbers.

*zero vehicle map

### Veterans

Region 8 has a veteran population of 19,238, accounting for 6.4 percent of the total population, which is close to the state and national percentages (6.02 percent and 7.49 percent respectively). The table below shows the demographic totals and percentages for Region 8 by county.

<rpc-table url="Veteran_2019.csv"
  table-title="Veteran Population"
  source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B21001"
  source-url="https://data.census.gov/cedsci/table?q=B21001&g=0100000US_0400000US17_0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183_400C100US15211,22204,22717&tid=ACSDT5Y2019.B21001"
  text-alignment="l,r"></rpc-table>

The largest veteran population resides in Champaign County (3,676), followed by Coles County (3,188). In terms of percentage, Shelby County is highest at 7.88 percent followed by Edgar County (7.55 percent) and Piatt County (7.11 percent). Champaign County has the smallest proportion of veteran residents at 5.64 percent, followed by Vermillion County with 5.99 percent.

### Minority Populations

#### Hispanic/Latino Population

Region 8 has a Hispanic/Latino population of 8,406, making up 4.01 percent of the total population, which is extremely low compared with the state and national percentages (16.96 percent and 17.81 percent respectively). The table below shows the demographic totals and percentages for Region 8 by county.

The largest Hispanic/Latino population resides in Champaign County (2,637) followed by Douglas County (1,424), and Vermilion County (1,319). In terms of percentage, Douglas County is highest at 7.22 percent followed by Champaign County (4.05 percent) and Vermilion County (4.16 percent). Macon County has the smallest proportion of Hispanic/Latino residents at 0.56 percent, followed by Cumberland and Shelby Counties with 1.05 percent and 1.04 percent respectively.

<iframe src="https://maps.ccrpc.org/hispanicmap/" width="100%" height="600" allowfullscreen="true"></iframe>

<rpc-table url="Hispanic_2019.csv"
  table-title="Hispanic/Latino Population"
  source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B03002"
  source-url="https://data.census.gov/cedsci/table?q=B03002&g=0100000US_0400000US17_0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183_400C100US15211,22204,22717&tid=ACSDT5Y2019.B03002"
  text-alignment="l,r"></rpc-table>

#### Black/African American Population

Region 8 has a Black/African American population of 8,274, accounting for 2.75 percent of the total population, which is extremely low compared with the state and national percentages (15.28 percent and 13.97 percent respectively). The table below shows the demographic totals and percentages for Region 8 by county.

<iframe src="https://maps.ccrpc.org/blackmap/" width="100%" height="600" allowfullscreen="true"></iframe>

<rpc-table url="Black_2019.csv"
  table-title="Black/African American Population"
  source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B02009"
  source-url="hhttps://data.census.gov/cedsci/table?q=B02009&g=0100000US_0400000US17_0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183_400C100US15211,22204,22717&tid=ACSDT5Y2019.B02009"
  text-alignment="l,r"></rpc-table>


The largest Black or African American population resides in Champaign County (4,066) followed by Coles County (2,448). In terms of percentage, Champaign County has the highest proportion at 6.24 percent followed by Coles County with 4.73 percent. DeWitt and Edgar Counties have the lowest percentage of Black or African American residents, at 0.55 and 0.57 percent respectively.
