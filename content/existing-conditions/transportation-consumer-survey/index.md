---
title: "Transportation Consumer Survey"
draft: false
weight: 20
---
Last updated in July 2022

#### Question 1: For which purposes do you use your agency's transportation services?

Respondents use Region 8 public transportation agencies for medical appointments the most frequently at 29 percent. Employment, shopping, and social/recreational trips come in as second most frequent at 19, 17, and 16 percent respectively.


<rpc-table url="q1table.csv"
  text-alignment="l,r"></rpc-table>

  <rpc-chart url="q1chart.csv"
    chart-title="Trip Type"
    type="horizontalBar"
    stacked="false"
    description="Other responses included: DT program services, volunteering, and going to the courthouse."
    aspect-ratio="3"></rpc-chart>


#### Question 2: Do you use any other transportation services within the community?

Of the responses to this question, over 90 percent only use one transportation service within their community.

<rpc-table url="q2table.csv"
  text-alignment="l,r"></rpc-table>

  <rpc-chart url="q2chart.csv"
    chart-title="Do you use any other transportation services within the community?"
    type="pie"
    description="Blank responses not included in the above chart."
    caption="Blank responses not included in chart."
    aspect-ratio="3"></rpc-chart>

#### Question 3: How often do you use the agency's transportation services?

Most respondents ride two to three days a week at 33.5 percent. The second highest frequency of trips was about even for less than once a month and four to five days at 17.3 and 17 percent respectively. Three respondents had not yet ridden their community's public transportation system.

<rpc-table url="q3table.csv"
  text-alignment="l,r"></rpc-table>

  <rpc-chart url="q3chart.csv"
    chart-title="Trip Frequency"
    type="horizontalBar"
    stacked="false"
    aspect-ratio="3"></rpc-chart>

#### Question 4: Are there obstacles preventing you from using transportation services more often?

The majority of respondents cited no obstacles to using public transit services at 42.2 percent. Passengers listed system hours of operation as the second highest obstacle to service usage. Many passenger want the ability to travel on weekends and during the evenings, while most Region 8 public transportation agencies operate Monday to Friday 8am to 5pm.

<rpc-table url="q4table.csv"
    text-alignment="l,r"></rpc-table>
*Total equals more than 252 because this question elicited multiple responses.*

<rpc-chart url="q4chart.csv"
    chart-title="Obstacles to Riding"
    type="horizontalBar"
    stacked="false"
    description="Blank responses not included in the above chart."
    aspect-ratio="3"></rpc-chart>

#### Question 5: What is your overall satisfaction with the agency's transportation services?

Respondents reported a generally high level of satisfaction with their public transportation agency.

<rpc-table url="q5table.csv"
    text-alignment="l,r"></rpc-table>

<rpc-chart url="q5chart.csv"
    chart-title="Passenger Satisfaction"
    type="pie"
    description="Blank responses not included in the above chart."
    aspect-ratio="3"></rpc-chart>

#### Question 6: Are you able to travel everywhere you would like to within the community?

Nearly 90 percent of those who responded to this question answered that they could travel anywhere in the community with their local public transportation service.

<rpc-table url="q6table.csv"
    text-alignment="l,r"></rpc-table>

<rpc-chart url="q6chart.csv"
    chart-title="Ability to Travel Everywhere Within Community"
    type="pie"
    description="Blank responses not included in the above chart."
    aspect-ratio="3"></rpc-chart>

##### Respondents who do not think they can travel everywhere with the local public transportation agency listed the following places they would like to travel:
- "Allerton and long grove neighborhood in Champaign"
- "Champaign, Springfield, Arthur, Tuscola and Gordyville"
- "Church on Sunday"

#### Question 7: How could the agency and/or the community better meet your transportation needs?

<rpc-table url="q7table.csv"
    text-alignment="l,r"></rpc-table>

#### Question 8: What do you see as the greatest barrier to mobility in the community?

The lack of information about transportation options was the highest barrier to mobility at nearly ten percent.

<rpc-table url="q8table.csv"
    text-alignment="l,r"></rpc-table>

<rpc-chart url="q8chart.csv"
    chart-title="Greatest Barrier to Mobility"
    type="horizontalBar"
    x-min="0"
    stacked="false"
    aspect-ratio="3"></rpc-chart>

#### Question 9: What is your age?

<rpc-table url="q9table.csv"
    text-alignment="l,r"></rpc-table>

<rpc-chart url="q9chart.csv"
    chart-title="Respondent Age"
    type="pie"
    description="Blank responses not included in the above chart."
    aspect-ratio="3"></rpc-chart>

#### Question 10: Do you have a physical disability?

<rpc-table url="q10table.csv"
    text-alignment="l,r"></rpc-table>

<rpc-chart url="q10chart.csv"
    chart-title="Respondent Ability"
    type="pie"
    description="Blank responses not included in the above chart."
    aspect-ratio="3"></rpc-chart>

#### Question 11: How do you think transportation coordination could be better in the community?

<rpc-table url="q11table.csv"
    text-alignment="l,r"></rpc-table>

#### Question 12: How do you think transportation coordination could be better in the community?

<rpc-table url="q12table.csv"
    text-alignment="l,r"></rpc-table>

#### Question 13: Are there any other issues or concerns you would like to share about your transportation experience?

<rpc-table url="q13table.csv"
    text-alignment="l,r"></rpc-table>

#### Question 14: If you do use public transportation, rate your overall experience with the issues below by circling 0 – 4.

<rpc-chart url="q14chart.csv"
    chart-title="Areas Rated"
    type="horizontalBar"
    x-min="0"
    x-max="4"
    stacked="false"
    aspect-ratio="3"></rpc-chart>

#### Question 14b: From transit customer service issues listed above, what are the most important to you?

<rpc-table url="q114btable.csv"
    text-alignment="l,r"></rpc-table>

<rpc-chart url="q14bchart.csv"
    chart-title="Areas Rated - Most Important"
    type="horizontalBar"
    stacked="false"
    aspect-ratio="3"></rpc-chart>
