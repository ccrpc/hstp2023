---
title: "County Demographics"
draft: false
weight: 40
---
### Champaign County

* Total Population: 65,167 (non-urbanized only)
* Total Area: 998 square miles
* County Seat: Urbana, IL

<rpc-table url="Champaign_overall_2019.csv"
  table-title="Champaign County Overall Demographics"
  text-alignment="l,r"></rpc-table>

  <rpc-chart url="Champaign_age_2019.csv"
    chart-title="Champaign County Demographics by Age"
    type="horizontalBar"
    stacked="true"
    aspect-ratio="4"></rpc-chart>


<rpc-chart url="Champaign_disability_2019.csv"
chart-title="Champaign County Population with a Disability"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B18101"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B18101&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B18101"
type="pie"></rpc-chart>


<rpc-chart url="Champaign_poverty_2019.csv"
chart-title="Champaign County Population Below the Poverty Level"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B17001"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B17001&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B17001"
type="pie"></rpc-chart>


<rpc-chart url="Champaign_veh_2019.csv"
chart-title="Zero Vehicle Households in Champaign County"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B08201"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B08201&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B08201"
type="pie"></rpc-chart>

#### Age

 Mahomet is home to the highest number of older adults (60+). Rantoul, St. Joseph, and Thomasboro also have significant numbers of older adults, as well as the unincorporated areas in the northwest and southeast Champaign County. The unincorporated areas north of the urbanized area also have a notable number of older adults.

#### Population with Disabilities

 The block groups where the highest number of households with an occupant with a disability are located in Mahomet and St. Joseph. Noteworthy numbers are also in Rantoul, Thomasboro, the unincorporated area north of the urbanized area, and the northeast corner of the county.

#### Low-Income Population
 Low-income households are most prominent in Mahomet, St. Joseph, and Rantoul. Aside from those areas, the distribution of low-income households is relatively even throughout Champaign County.

#### Zero-Vehicle Households
Nearly all block groups in Champaign County have less than 33 households without a vehicle. Rantoul and Mahomet have the most zero-vehicle households, however St. Joseph and the unincorporated area north of the urbanized area also have a notable number of these households.

#### Hispanic/Latino Population
The largest number of Hispanic/Latino residents live in Rantoul, closely followed by Mahomet. The unincorporated areas west and southeast of the urbanized area, and the northeast corner of the county also have significant Hispanic/Latino populations.

#### Black/African American Population
Rantoul and Mahomet account for the majority of the Black/African American population in Champaign County, however there are also significant Black/African American populations in the unincorporated areas north and west of the urbanized area.

#### Champaign County in Summary
Synthesizing the distribution of each of these populations considered as more likely to be dependent on transit services, the most affected rural communities in Champaign County are Mahomet, Rantoul, and St. Joseph. Trips to-and-from and within Rantoul account for roughly 60 percent of C-CARTS service. Rantoul also has the largest number of registered riders, followed by Mahomet and St. Joseph (excluding the urbanized area). Although three of the top employers are in Rantoul, and one is in Mahomet, most of the jobs in Champaign County are in the urbanized area. The same is true for medical facilities, shopping, and public services.


### Clark County

* Total Population: 15,836
* Total Area: 505 square miles
* County Seat: Marshall, IL


<rpc-table url="Clark_overall_2019.csv"
  table-title="Clark County Overall Demographics"
  text-alignment="l,r"></rpc-table>

  <rpc-chart url="Clark_age_2019.csv"
    chart-title="Clark County Demographics by Age"
    type="horizontalBar"
    stacked="true"
    aspect-ratio="4"></rpc-chart>

<rpc-chart url="Clark_disability_2019.csv"
chart-title="Clark County Population with a Disability"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B18101"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B18101&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B18101"
type="pie"></rpc-chart>


<rpc-chart url="Clark_poverty_2019.csv"
chart-title="Clark County Population Below the Poverty Level"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B17001"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B17001&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B17001"
type="pie"></rpc-chart>


<rpc-chart url="Clark_veh_2019.csv"
chart-title="Zero Vehicle Households in Clark County"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B08201"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B08201&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B08201"
type="pie"></rpc-chart>

#### Age
The block groups with the highest number of older adults (60+) are in the northwest and southwest quadrants of the County, as well as one block group in Marshall. Half of the block groups in Clark County have more than 187 older adults.

#### Population with Disabilities
Nearly all block groups in Clark County have less than 68 households where an occupant lives with a disability. Casey, Marshall, and Westfield have noteworthy numbers of these households, as well as the block group east of Marshall and the northwest and southwest parts of the county.

#### Low-Income Population
Low-income households are distributed fairly evenly throughout Clark County. Casey and Marshall have the highest number of low-income households, while the block group north of Marshall and the southeast corner of the two block groups in the southeast corner of the county have less than 16 low-income households each.

#### Zero-Vehicle Households
Half of the block groups in Clark County have less than nine households without a vehicle. Casey, Marshall, Martinsville, Westfield, and West Union have notable numbers of zero-vehicle households.

#### Hispanic/Latino Population
Only four block groups have a Hispanic/Latino population over 14. Casey, Martinsville, Marshall, and directly west of Marshall have the largest Hispanic/Latino population in Clark County.

#### Black/African American Population
Every block group in Clark County has less than 20 Black/African American individuals.

#### Clark County in Summary
Casey, Marshall, Martinsville, and Westfield in Clark County have the largest numbers of populations considered as more likely to be dependent on transit services. Of these four, three municipalities have both a grocery store and a medical facility; Westfield has neither a grocery store nor medical facility. The distribution of jobs in Clark County is fortunate given the concentration of the target populations: six of the top employers are in Marshall, three are in Martinsville, and the remaining two are in Casey. These factors indicate that vital services are likely accessible to more Clark County residents than in other counties in Region 8.


### Coles County

* Total Population: 51,736
* Total Area: 510 square miles
* County Seat: Charleston, IL


<rpc-table url="Coles_overall_2019.csv"
  table-title="Coles County Overall Demographics"
  text-alignment="l,r"></rpc-table>

<rpc-chart url="Coles_age_2019.csv"
    chart-title="Coles County Demographics by Age"
    type="horizontalBar"
    stacked="true"
    aspect-ratio="4"></rpc-chart>


<rpc-chart url="Coles_disability_2019.csv"
chart-title="Coles County Population with a Disability"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B18101"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B18101&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B18101"
type="pie"></rpc-chart>


<rpc-chart url="Coles_poverty_2019.csv"
chart-title="Coles County Population Below the Poverty Level"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B17001"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B17001&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B17001"
type="pie"></rpc-chart>


<rpc-chart url="Coles_veh_2019.csv"
chart-title="Zero Vehicle Households in Coles County"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B08201"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B08201&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B08201"
type="pie"></rpc-chart>

#### Age
With the exception of Oakland, the northern part of Coles County has a less significant presence of older adults (60+). Older adult populations are concentrated in Charleston. Three block groups are home to between 491 and 1,017 older adults each.

#### Population with Disabilities
Over half of the block groups in Coles County have at least 68 households where an occupant lives with a disability. Charleston and Mattoon have the largest number of these households, with a significant number of households in the southwestern portion of the county as well.

#### Low-Income Population
Low-income households are most prominent in Charleston and Mattoon, as well as the unincorporated areas to the northeast of Charleston and the south of Mattoon. Most of these block groups have between 51 and 174 low-income households, and six block groups have between 175 and 315.

#### Zero-Vehicle Households
The largest number of zero-vehicle households are located on the west side of Charleston, the east side of Mattoon, and the corridor between those communities. This distribution is similar to that of low-income households.

#### Hispanic/Latino Population
The majority of block groups in Coles County have at least 15 Hispanic/Latino residents; concentrated mostly in Charleston, closely followed by Mattoon and their surrounding areas. There is also a notable Hispanic/Latino population in the northeast corner of the county.

#### Black/African American Population
Charleston accounts for most of the Black/African American population in Coles County, however there are also a few block groups in Mattoon, and two others between Charleston and Mattoon, that have between 20 and 71 Black/African American residents.

#### Coles County in Summary
Mattoon and Charleston have the largest numbers of populations considered as more likely to be dependent on transit services. Within this area, the block groups between Charleston and Mattoon have elevated concentrations of most of the target populations. Block groups to the south and east of Charleston also have some of the largest populations. Coles County has ten block groups that have the highest concentrations of target populations for the whole of Region 8. These higher concentrations occur in the older adult population, low income population, and the households with occupants with a disability. These areas are also moderately high in numbers for the other target populations. The Charleston – Mattoon area is well-served with vital services and employment. Other rural communities within Coles County have lower concentrations of the target populations, but they are also lacking in employment opportunities and vital services. Humboldt, Ashmore, and Lerna all have lower target population concentrations with no top employers, no medical services, and no grocery stores. Oakland has lower target population concentrations, but people in this community do have limited access to medical facilities and grocery stores.

### Cumberland County

* Total Population: 10,865
* Total Area: 347 square miles
* County Seat: Toledo, IL


<rpc-table url="Cumberland_overall_2019.csv"
  table-title="Cumberland County Overall Demographics"
  text-alignment="l,r"></rpc-table>

<rpc-chart url="Cumberland_age_2019.csv"
    chart-title="Cumberland County Demographics by Age"
    type="horizontalBar"
    stacked="true"
    aspect-ratio="4"></rpc-chart>


<rpc-chart url="Cumberland_disability_2019.csv"
chart-title="Cumberland County Population with a Disability"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B18101"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B18101&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B18101"
type="pie"></rpc-chart>


<rpc-chart url="Cumberland_poverty_2019.csv"
chart-title="Cumberland County Population Below the Poverty Level"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B17001"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B17001&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B17001"
type="pie"></rpc-chart>


<rpc-chart url="Cumberland_veh_2019.csv"
chart-title="Zero Vehicle Households in Cumberland County"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B08201"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B08201&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B08201"
type="pie"></rpc-chart>

#### Age
Most block groups in Cumberland County have an older adult (60+) population between 187 and 283. The northwestern quadrant of the county has the largest population of older adults with one block group alone having between 491 and 1,017.

#### Population with Disabilities
Half of the block groups in Cumberland County have at least 115 households where an occupant lives with a disability. The largest number of these households are located in the west and southwest parts of the county.

#### Low-Income Population
With the exception of one, all of Cumberland County’s block groups have over 17 low-income households. Toledo has the highest number of low-income households, between 101 and 174. The northwestern corner of the county and the unincorporated area to the southeast of Toledo also have significant numbers of low-income households.

#### Zero-Vehicle Households
Toledo has the highest amount of zero-vehicle households, followed by the block groups directly surrounding it. This distribution is similar to that of low-income households.

#### Hispanic/Latino Population
Neoga has between 49 and 93 Hispanic/Latino residents, and the block group directly south of Toledo has between 15 and 48. All other block groups have a Hispanic/Latino population of 14 or less.

#### Black/African American Population
Every block group in Cumberland County has less than 20 Black/African American individuals.

#### Cumberland County in Summary
Neoga, Greenup, and Toledo are Cumberland County’s largest population areas and roughly designate the areas also more likely to be dependent on transit services. Since the population density in Cumberland County’s most populated areas is moderately low, the block groups around the incorporated areas are larger. These larger block group designations, create a challenge specifying target population locations. The block groups nearest to Neoga and Greenup have largest numbers of the target populations, however most of Cumberland County from Neoga to Greenup has high concentrations of the target populations. Each of the main municipalities have both a grocery store and a medical facility. There are no grocery stores or medical facilities outside of these municipalities. Each of Cumberland County top employers are within its main municipalities. The vital services are also located almost solely within the top three municipalities. This equalized distribution of target populations suggests many Cumberland County transit dependent residents may be also residing outside of the municipalities.


### DeWitt County

* Total Population:16,042
* Total Area: 405 square miles
* County Seat: Clinton, IL


<rpc-table url="De Witt_overall_2019.csv"
  table-title="DeWitt County Overall Demographics"
  text-alignment="l,r"></rpc-table>

<rpc-chart url="De Witt_age_2019.csv"
    chart-title="DeWitt County Demographics by Age"
    type="horizontalBar"
    stacked="true"
    aspect-ratio="4"></rpc-chart>


<rpc-chart url="De Witt_disability_2019.csv"
chart-title="DeWitt County Population with a Disability"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B18101"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B18101&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B18101"
type="pie"></rpc-chart>


<rpc-chart url="De Witt_poverty_2019.csv"
chart-title="DeWitt County Population Below the Poverty Level"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B17001"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B17001&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B17001"
type="pie"></rpc-chart>


<rpc-chart url="De Witt_veh_2019.csv"
chart-title="Zero Vehicle Households in DeWitt County"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B08201"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B08201&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B08201"
type="pie"></rpc-chart>

#### Age
Clinton accounts for most of the older adult (60+) population in DeWitt County, however there are also a few block groups in the northeast portion of the county that are each home to between 284 older adults.

#### Population with Disabilities
Aside from one, all block groups in DeWitt County have at least 68 households where an occupant lives with a disability. The largest number of these households are located southeast of Clinton, and in the eastern portion of the county.

#### Low-Income Population
Aside from Clinton, where most block groups have between 151 and 174, low-income households are distributed evenly throughout DeWitt County. All of the block groups outside Clinton have between 17 and 50 low-income households, with the exception of one in the northeast corner which has between 51 and 100.

#### Zero-Vehicle Households
The largest amount of zero-vehicle households is located in the northeast corner of DeWitt County, followed by the block groups in Clinton, similar to the distribution of low-income households.

#### Hispanic/Latino Population
The Hispanic/Latino population in DeWitt County is concentrated in Clinton and the area to the northwest of Clinton. All other block groups have less than 15 Hispanic/Latino residents.

#### Black/African American Population
With the exception of one block group in Clinton, there are no block groups in DeWitt County with more than 20 Black/African American individuals.

#### DeWitt County in Summary
Clinton and Farmer City have the largest numbers of populations considered as more likely to be dependent on transit services. Clinton has the highest concentrations of the target populations for DeWitt County. One southeast block group is in the highest range for households with an occupant with a disability and another southwest block group is in the highest range for the older adult population when compared with the whole of Region 8. The area around Farmer City also significant numbers of low income households, zero car households, households with an occupant with a disability, and older adult population. Both of Clinton and Farmer City are well-served with vital services. There were no major employment locations in Farmer City. Nearly all the major employers were located in and around Clinton. The other rural communities, Waynesville, Wapella, DeWitt, Weldon, and Kenney, all have a relatively lower concentration of the target populations, and offer severely limited vital services and employment options. Residents of these places likely need to travel to Clinton or Farmer City to meet their basic needs.


### Douglas County

* Total Population: 19,714
* Total Area: 417 square miles
* County Seat: Tuscola, IL


<rpc-table url="Douglas_overall_2019.csv"
  table-title="Douglas County Overall Demographics"
  text-alignment="l,r"></rpc-table>

<rpc-chart url="Douglas_age_2019.csv"
    chart-title="Douglas County Demographics by Age"
    type="horizontalBar"
    stacked="true"
    aspect-ratio="4"></rpc-chart>


<rpc-chart url="Douglas_disability_2019.csv"
chart-title="Douglas County Population with a Disability"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B18101"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B18101&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B18101"
type="pie"></rpc-chart>


<rpc-chart url="Douglas_poverty_2019.csv"
chart-title="Douglas County Population Below the Poverty Level"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B17001"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B17001&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B17001"
type="pie"></rpc-chart>


<rpc-chart url="Douglas_veh_2019.csv"
chart-title="Zero Vehicle Households in Douglas County"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B08201"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B08201&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B08201"
type="pie"></rpc-chart>

#### Age
The population of older adults (60+) is distributed fairly evenly throughout Douglas County. Nearly all block groups have between 187 and 283 older adults. Concentrations are higher in and around Arthur, Tuscola, and Villa Grove.

#### Population with Disabilities
Distribution of households where an occupant lives with a disability is fairly even throughout Douglas County, although there are larger numbers located around Tuscola and Arcola.

#### Low-Income Population
Distribution of low-income households is fairly even throughout Douglas County. Nearly all block groups have between 17 and 50 low-income households, four block groups have between 51 and 100, and one block group has less than 17 low-income households.

#### Zero-Vehicle Households
The southwest corner of Douglas County has the highest number of zero-vehicle households, followed by the block groups around Tuscola and near Villa Grove.

#### Hispanic/Latino Population
Arcola and Tuscola account for most of the Hispanic/Latino population in Douglas County, however there are also a noteworthy number of Hispanic/Latino individuals near Villa Grove, in Arthur, and in the southwest corner of the county.

#### Black/African American Population
There are only two block groups in Douglas County with more than 20 Black/African American individuals: one in Tuscola, and one in Eastern Douglas County.

#### Douglas County in Summary
Tuscola, Arcola, Arthur, and Villa Grove have the highest concentrations of populations considered more likely to be dependent on transit services within Douglas County. Over half of the target populations are distributed relatively evenly. The older adult population and households with an occupant with a disability have moderate concentrations around Tuscola, Villa Grove, and Arthur. The target populations that are exceptional and have a more disproportionate distribution are zero vehicle households and the Hispanic or Latino population. Three block groups within Douglas County have target populations in the highest ranges for all of Region 8. The block group containing Arcola, and the block group west of Arcola each have absolute highest numbers of Hispanic or Latino residents. The southwest most block group within Douglas County has the absolute highest number of zero vehicle households for Region 8. The block group between Tuscola and Arcola does not contain any target populations in the highest concentration ranges, however, it has the highest cumulative composition of target population concentrations within Douglas County. Tuscola, Arcola, and Arthur are all well-served with vital services and employment opportunities. Villa Grove and Newman have basic access to medical facilities and grocery stores without any major employers. The rural communities of Garrett, Camargo, and Hindsboro provide the least level of vital services, but also correspond to lower concentrations of target populations.


### Edgar County

* Total Population: 17,539
* Total Area: 624 square miles
* County Seat: Paris, IL


<rpc-table url="Edgar_overall_2019.csv"
  table-title="Edgar County Overall Demographics"
  text-alignment="l,r"></rpc-table>

<rpc-chart url="Edgar_age_2019.csv"
    chart-title="Edgar County Demographics by Age"
    type="horizontalBar"
    stacked="true"
    aspect-ratio="4"></rpc-chart>


<rpc-chart url="Edgar_disability_2019.csv"
chart-title="Edgar County Population with a Disability"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B18101"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B18101&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B18101"
type="pie"></rpc-chart>


<rpc-chart url="Edgar_poverty_2019.csv"
chart-title="Edgar County Population Below the Poverty Level"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B17001"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B17001&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B17001"
type="pie"></rpc-chart>


<rpc-chart url="Edgar_veh_2019.csv"
chart-title="Zero Vehicle Households in Edgar County"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B08201"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B08201&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B08201"
type="pie"></rpc-chart>

#### Age
Paris is home to the highest number of older adults (60+). The unincorporated areas in the northeast corner and to the southwest of Paris also have significant older adult populations. Most other block groups have at least 187 older adults.

#### Population with Disabilities
Distribution of households where an occupant lives with a disability is fairly even throughout Edgar County, although there are larger numbers located around Paris and the northeastern corner of the county. Nearly all block groups have at least 68 households with an occupant who has a disability.

#### Low-Income Population
Paris has the largest number of low-income households, where some block groups have between 101 and 315. In the north and west portions of the county have between 51 and 100 low-income households, whereas in the east block groups each have between 17 and 50.

#### Zero-Vehicle Households
Zero-vehicle household distribution for Edgar County closely resembles that of low-income households. Paris has the highest number of zero-vehicle households, followed by the north and west portion of the county.

#### Hispanic/Latino Population
The Hispanic/Latino population in Edgar County is concentrated in Paris and the area to the northwest of Paris. All other block groups have less than 15 Hispanic/Latino residents.

#### Black/African American Population
There are only two block groups in Edgar County with more than 20 Black/African American individuals: one in Paris, and one southwest of Paris.

#### Edgar County in Summary
Paris, Chrisman, and Kansas have the largest populations within Edgar County. The block groups surrounding Paris, especially on the east side, are the most likely areas to be dependent on transit services within Edgar County. Other areas of high dependency include the block group between Paris and Kansas and the block groups that contain the incorporated areas of Hume, Metcalf, and Chrisman. Of the three largest incorporated areas, Paris and Chrisman have both a grocery store and a medical facility. Kansas does not have a grocery store, but it is home to Christensen Farms, the only top employer for that location. Edgar County contains all its mid-sized top employers around Paris, with the exception of one top employer in Kansas and one top employer in Chrisman. The high concentration of top employers and vital services in Paris matches well with the high demand for transit near the city boundaries, but it does not address the many other target populations that are found throughout the county, not only in and around Chrisman and Kansas, but also near Hume, Metcalf, and Vermilion.


### Macon County

* Total Population: 18,793 (non-urbanized only)
* Total Area: 586 square miles
* County Seat: Decatur, IL


<rpc-table url="Macon_overall_2019.csv"
  table-title="Macon County Overall Demographics"
  text-alignment="l,r"></rpc-table>

<rpc-chart url="Macon_age_2019.csv"
    chart-title="Macon County Demographics by Age"
    type="horizontalBar"
    stacked="true"
    aspect-ratio="4"></rpc-chart>


<rpc-chart url="Macon_disability_2019.csv"
chart-title="Macon County Population with a Disability"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B18101"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B18101&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B18101"
type="pie"></rpc-chart>


<rpc-chart url="Macon_poverty_2019.csv"
chart-title="Macon County Population Below the Poverty Level"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B17001"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B17001&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B17001"
type="pie"></rpc-chart>


<rpc-chart url="Macon_veh_2019.csv"
chart-title="Zero Vehicle Households in Macon County"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B08201"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B08201&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B08201"
type="pie"></rpc-chart>

#### Age
The population of older adults (60+) is distributed fairly evenly throughout Macon County, the outlier being the block group northeast of the urbanized area, which has the largest number of residents over 60. The two block groups in the northwest corner of the county as well as the southern block groups each have an older adult population between 187 and 283.

#### Population with Disabilities
Block groups located to the south and northeast of the urbanized area, as well as near Maroa, each have between 115 and 180 households with an occupant living with a disability. Block groups in the northern portion of the county and the southwest quadrant have between 68 and 114 of these households each.

#### Low-Income Population
Over half of the block groups in Macon County have less than 17 low-income households. The most prominent occurrences are in the northwest corner and the southwest corner, where each block group has between 51 and 100 low-income households.

#### Zero-Vehicle Households
Zero-vehicle households are most prominent in southwestern Macon County, however there are also notable numbers in Niantic, Maroa, and the northeast corner of the county.

#### Hispanic/Latino Population
The largest number of Hispanic/Latino residents live in the Northwest corner of Macon County. Three block groups are home to between 15 and 48 Hispanic/Latino individuals each: directly north, directly west, and directly east of the urbanized area.

#### Black/African American Population
Most block groups in Macon County have less than 20 Black/African American residents. Four block groups have between 20 and 71 Black/African American residents: the northwest and northeast corners, the block group directly south of the urbanized area, and the southwest block group partially included in the urbanized area.

#### Macon County in Summary
Synthesizing the distribution of each of these populations considered as more likely to be dependent on transit services, the most affected rural communities in Macon County are directly northeast of the urbanized area, and to the south and southwest. All 23 of the top employers, two of which are in Mt. Zion and the rest in Decatur, are located within Macon County’s urbanized area. Most medical facilities, shopping, and public services are also located within the urbanized area.


### Moultrie County

* Total Population: 14,703
* Total Area: 344 square miles
* County Seat: Sullivan, IL


<rpc-table url="Moultrie_overall_2019.csv"
  table-title="Moultrie County Overall Demographics"
  text-alignment="l,r"></rpc-table>

<rpc-chart url="Moultrie_age_2019.csv"
    chart-title="Moultrie County Demographics by Age"
    type="horizontalBar"
    stacked="true"
    aspect-ratio="4"></rpc-chart>


<rpc-chart url="Moultrie_disability_2019.csv"
chart-title="Moultrie County Population with a Disability"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B18101"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B18101&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B18101"
type="pie"></rpc-chart>


<rpc-chart url="Moultrie_poverty_2019.csv"
chart-title="Moultrie County Population Below the Poverty Level"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B17001"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B17001&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B17001"
type="pie"></rpc-chart>


<rpc-chart url="Moultrie_veh_2019.csv"
chart-title="Zero Vehicle Households in Moultrie County"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B08201"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B08201&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B08201"
type="pie"></rpc-chart>

#### Age
Sullivan is home to the largest group of older adults (60+), followed by Allenville and an unincorporated area to the west of Sullivan. The northwest quadrant of Moultrie County has the lowest older adult population.

#### Population with Disabilities
Nearly all block groups in Moultrie County have less between 68 and 114 households where an occupant lives with a disability. The number of these households belonging to block groups in Arthur and Sullivan is between 115 and 362 each.

#### Low-Income Population
Low-income households are most prominent in Sullivan and the unincorporated areas to the west and north of Sullivan. The block group in Arthur and the southernmost block group each have less than 17 low-income households. The remainder of the block groups have between 17 and 50 low-income households.

#### Zero-Vehicle Households
Zero-vehicle households are fairly evenly distributed throughout Moultrie County, aside from the northeastern corner near Arthur where there are between 166 and 261 zero-vehicle households.

#### Hispanic/Latino Population
The Hispanic/Latino population in Moultrie County is concentrated in and around Sullivan and Allenville, and in Arthur. All other block groups have less than 15 Hispanic/Latino residents.

#### Black/African American Population
With the exception of three block groups (Arthur, part of Sullivan, and the southernmost block group), all block groups in Moultrie County have less than 20 Black/African American residents.

#### Moultrie County in Summary
Sullivan, Allenville, and Arthur are locations within Moultrie County which have the largest concentrations of the target populations that may be more likely dependent on transit services. Of these locations Sullivan and Arthur both have adequate vital services: medical facilities, groceries stores, and major employers. Vital services are not adequate for Allenville. The block group south of Sullivan is one of the block groups with the highest number of older adults and the highest number households with an occupant with a disability for Region 8. The large block group northeast of Sullivan and west of Arthur is in the greatest range of zero vehicle households of all Region as well due to the local Amish communities near Arthur. Lovington, Bethany, and Gays are other rural communities within Moultrie County which have low to moderate concentrations of the target populations. Lovington and Bethany are adequately served by medical and grocery services, but Gays does not have access to these vital services. They all lack major employers, and many may still need access to transit services to reach their locations of employment or to find work.


### Piatt County

* Total Population: 16,427
* Total Area: 439 square miles
* County Seat: Monticello, IL


<rpc-table url="Piatt_overall_2019.csv"
  table-title="Piatt County Overall Demographics"
  text-alignment="l,r"></rpc-table>

<rpc-chart url="Piatt_age_2019.csv"
    chart-title="Piatt County Demographics by Age"
    type="horizontalBar"
    stacked="true"
    aspect-ratio="4"></rpc-chart>


<rpc-chart url="Piatt_disability_2019.csv"
chart-title="Piatt County Population with a Disability"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B18101"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B18101&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B18101"
type="pie"></rpc-chart>


<rpc-chart url="Piatt_poverty_2019.csv"
chart-title="Piatt County Population Below the Poverty Level"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B17001"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B17001&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B17001"
type="pie"></rpc-chart>


<rpc-chart url="Piatt_veh_2019.csv"
chart-title="Zero Vehicle Households in Piatt County"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B08201"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B08201&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B08201"
type="pie"></rpc-chart>

#### Age
Monticello is home to the largest group of older adults (60+) in Piatt County. The remainder of the block groups in the northern part of the county have between 187 and 283 older adults each, whereas the majority of the southern block groups have between 67 and 186 older adults each.

#### Population with Disabilities
Monticello and the unincorporated area to its northeast have the greatest number of households where an occupant lives with a disability. There are at least 24 of these households in each of the remaining block groups with the exception of one.

#### Low-Income Population
Over half of Piatt County’s block groups have less than 17 low-income households. Monticello has the highest number of low-income households, between 101 and 174.

#### Zero-Vehicle Households
There are only six block groups in Piatt County with more than nine zero-vehicle households: Atwood, Bement, Cerro Gordo, DeLand, Hammond, and Monticello.

#### Hispanic/Latino Population
Monticello accounts for most of the Hispanic/Latino population in Piatt County. Outside of Monticello, there is only one block group with more than 14 Hispanic/Latino residents.

#### Black/African American Population
Nearly every block group in Piatt County has less than 20 Black/African American residents. The two exceptions are Cerro Gordo and one block group in Monticello.

#### Piatt County in Summary
Monticello periphery and the area around unincorporated White Heath are locations with the highest concentration of the target populations that may be more likely dependent on transit services. The Monticello periphery block group is in the highest ranges for Region 8 as a whole in two target populations: the older adult population and households with and occupant with a disability. It also has a considerable concentration of households living in poverty. The White Heath block group is in the highest range for Region 8 in one target population: households with an occupant with a disability. Many vital services and employment opportunities are available near Monticello, but for the block group including White Heath, these necessities are not adequate. Cerro Gordo and Bement are rural communities which have lower concentrations of the target populations, and access to basic vital services. LaPlace, Hammond, Atwood, Cisco, DeLand, and Mansfield are rural communities within Piatt County that have lower concentrations of the target populations, but also have limited vital services. These are also under-served communities that may not have medical facilities, groceries stores, and/or employment.


### Shelby County

* Total Population: 21,832
* Total Area: 768 square miles
* County Seat: Shelbyville, IL


<rpc-table url="Shelby_overall_2019.csv"
  table-title="Shelby County Overall Demographics"
  text-alignment="l,r"></rpc-table>

<rpc-chart url="Shelby_age_2019.csv"
    chart-title="Shelby County Demographics by Age"
    type="horizontalBar"
    stacked="true"
    aspect-ratio="4"></rpc-chart>


<rpc-chart url="Shelby_disability_2019.csv"
chart-title="Shelby County Population with a Disability"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B18101"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B18101&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B18101"
type="pie"></rpc-chart>


<rpc-chart url="Shelby_poverty_2019.csv"
chart-title="Shelby County Population Below the Poverty Level"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B17001"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B17001&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B17001"
type="pie"></rpc-chart>


<rpc-chart url="Shelby_veh_2019.csv"
chart-title="Zero Vehicle Households in Shelby County"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B08201"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B08201&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B08201"
type="pie"></rpc-chart>

#### Age
Most block groups in Shelby County have an older adult (60+) population between 67 and 283. Shelbyville accounts for most of the older adult population, however the Oconee area and Moweaqua also have notable older adult populations.

#### Population with Disabilities
The block groups with the highest number of households with an occupant with a disability are located in Shelbyville. Noteworthy numbers are also near Cowden and Stewardson.

#### Low-Income Population
Low-income households are most prominent in Shelbyville and Moweaqua, as well as around Cowden, Herrick, and Sigel. The remaining block groups, aside from three, have between 17 and 50 low-income households each.

#### Zero-Vehicle Households
Three block groups in Shelby County have between 33 and 77 zero-vehicle households, and the majority of the rest have less than ten. Outside of Shelbyville, the distribution of zero-vehicle households does not match the distribution of low-income households.

#### Hispanic/Latino Population
Nearly all block groups in Shelby County have less than 15 Hispanic/Latino residents. Shelbyville has one block group with a Hispanic/Latino population between 49 and 93, as well as one block group to the east. To the southwest, there is an additional block group with between 15 and 48 Hispanic/Latino individuals.

#### Black/African American Population
Most block groups in Shelby County have less than 20 Black/African American residents. Four block groups have between 20 and 71 Black/African American residents: the two block groups that cover Moweaqua, and the two block groups directly east of Shelbyville.

#### Shelby County in Summary
Shelbyville, Cowden, Windsor, and Moweaqua are locations within Shelby County which have the largest concentrations of these target populations that may be more likely dependent on transit services. Of these locations, each municipality has at least one medical facility and one grocery store. Cowden and Windsor are lacking any major employers. Nearly all the top employers are located in and around the Shelbyville area, with the exception of a retirement center in Moweaqua. One block group on the northwest side of Shelbyville has some of the highest numbers of both older adults and households with an occupant with a disability within Region 8.


### Vermillion County

* Total Population: 31,744 (non-urbanized area)
* Total Area: 901 square miles
* County Seat: Danville, IL


<rpc-table url="Vermillion_overall_2019.csv"
  table-title="Vermillion County Overall Demographics"
  text-alignment="l,r"></rpc-table>

<rpc-chart url="Vermillion_age_2019.csv"
    chart-title="Vermillion County Demographics by Age"
    type="horizontalBar"
    stacked="true"
    aspect-ratio="4"></rpc-chart>


<rpc-chart url="Vermillion_disability_2019.csv"
chart-title="Vermillion County Population with a Disability"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B18101"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B18101&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B18101"
type="pie"></rpc-chart>


<rpc-chart url="Vermillion_poverty_2019.csv"
chart-title="Vermillion County Population Below the Poverty Level"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B17001"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B17001&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B17001"
type="pie"></rpc-chart>


<rpc-chart url="Vermillion_veh_2019.csv"
chart-title="Zero Vehicle Households in Vermillion County"
source="U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B08201"
aspect-ratio="3"
source-url="https://data.census.gov/cedsci/table?q=B08201&g=0500000US17019,17023,17029,17035,17039,17041,17045,17115,17139,17147,17173,17183&tid=ACSDT5Y2019.B08201"
type="pie"></rpc-chart>

#### Age
Vermilion County’s older adult (60+) population is generally concentrated in the north and northwest portion of the county, as well as the unincorporated areas to the west and southeast of the urbanized area where block groups have between 284 and 490 older adults each. Nearly all other block groups have at least 67 older adult residents.

#### Population with Disabilities
Hoopeston has the highest number of households where an occupant lives with a disability. Rossville and Oakwood have between 115 and 180 of these households, as well as one block group in western Vermilion County and the southernmost block group. The majority of the remaining block groups have 68 to 114 households where an occupant lives with a disability.

#### Low-Income Population
Low-income households are most prominent in Hoopeston, followed by Oakwood, Ridge Farm, and unincorporated areas in the northern and western portions of Vermilion County. Nearly all block groups have at least 17 low-income households.

#### Zero-Vehicle Households
Nearly all block groups in Vermilion County have less than 33 households without a vehicle. Hoopeston has the largest number of zero-vehicle households, followed by the unincorporated area southeast of the urbanized area. This distribution somewhat matches that of low-income households.

#### Hispanic/Latino Population
The largest number of Hispanic/Latino residents live in Hoopeston, closely followed by Bismarck and Oakwood. Noteworthy numbers of Hispanic/Latino individuals are also located near Rossville, Fithian, Muncie, Georgetown, and Olivet.

#### Black/African American Population
Most block groups in Vermilion County have less than 20 Black/African American residents. The exceptions to this are three block groups in Hoopeston, Oakwood, two block groups on the west, and a few block groups to the northeast and east of the urbanized area.

#### Vermillion County in Summary
Synthesizing the distribution of each of these populations considered as more likely to be dependent on transit services, the most affected rural communities in Vermilion County are Hoopeston, Oakwood, and Rossville. Of the 26 top employers in Vermilion County, 25 are in the urbanized area. The urbanized area also contains most of the county’s medical facilities, shopping, public services, and specialty service facilities. Hoopeston, Oakwood, and Rossville each have a medical clinic, and of the three Rossville is the only municipality without a grocery store. For the target populations, public transportation serves as a vital connection to these essential services.
