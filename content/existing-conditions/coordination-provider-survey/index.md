---
title: "Coordination Provider Survey"
draft: false
weight: 10
---
### Coordination Provider Survey

#### Local Coordination Efforts

#### Question 1: What do you see as the greatest barrier to coordination and mobility in your service area?

CCAR:

- Mass transit providers need more new vehicles to have the availability to serve our human service agency.

C-CARTS:

- The inability to travel between counties due to service area
- Bringing passenger assistants back and forth, such as for assisted living facilities, because it would interfere with service too much
-	Medical transportation requirements for assisted living facilities

CRIS Rural Mass Transit District:

- Covid-19 social distancing

Charleston Transitional Facility:

- Cost, insurance, liability, lack of capacity for public providers

Dial-A-Ride:

- The greatest barrier is funding and lack of resources for example, proving vehicles, hiring/training drivers, and driver’s salary.

Human Resources Center of Edgar and Clark:

- Lack of phones by clients; COVID restrictions on mixing groups of people; lack of open hours for local transit provider(s)

LifeLinks:

- The riders transported by Lifelinks are individuals with experience a serious and persistent mental illness - schizophrenia and bipolar disorders for example. With the stigma and lack of understanding, about the symptoms these disorders many are apprehensive and sometimes fearful to become involved. Paranoia, voices, etc. often interfere with their ability to comply in the process.

Macon Resources, Inc.:

- We provide transportation for the individuals in our Community Day Services (CDS) program only.  We do have some individuals that use the services to attend CDS.  Decatur Public Transit System (DPTS) has been very accommodating and have communicated well when changes happen.  The only barrier I am aware of from the people we serve is that there is not service on Sundays or after 7:15 pm on other day.  

Piattran:

- When serving out-lying cities/villages between counties, we get many calls from people in other Counties, stating they have been denied rides months out by their local provider and are seeking assistance getting to medical appointments.  They see us in their area when transporting riders into Piatt County, then we must explain that we cannot take a ride that doesn’t have one leg of the trip in Piatt County.  When we have worked with other transportation providers, for example, one of our regular riders was staying in another area for a week or two, the local provider wanted forms from us, that we do not use, a paratransit form of some sort.

- It seems the everyone provides service so differently and has different rules that it is hard to coordinate between other agencies.  The hours of operations are not standard, the level of service is not standard, and the fare rates are not standard.  When it comes to more private transportation services, there is little to no communication between agencies.  We see quite a few private transportation agencies in Piatt County, that we have no idea how they are even connected with a different provider.

Showbus:

- In the newest SHOW BUS county in Region 8, (Macon), most service providers are located in urban areas.  Some do outreach to rural areas, but the majority of work is done with an urban emphasis.  With our urban/rural counties, we have more difficulty obtaining service contracts.

RIDES Rural Mass Transit District:

- Currently, the fallout and reinventing of social distancing during COVID-19 will cause some great changes with human service and public transit coordination.

Shelby County Community Services:

- Rural setting

#### Question 2: What strengths do you see in coordination efforts of public and human service transportation in your service area?

CCAR:

- Good faith effort by all involved. Each service in our area understands each others' limitations.

C-CARTS:

- Coordination between some human service agencies with transportation providers for longer trips
-	Coordination for driver training planning and hosting

CRIS Rural Mass Transit District:

- Our DATS council meets regularity and communicates between the Rural and Urban systems as well as township transit, airport transit.

Charleston Transitional Facility:

-  (Coles County) Zipline is great addition to services
- Willingness to explore alternate options

Dial-A-Ride:

- Dial-A-Ride works very closely with Dialysis, hospitals, and social service agencies to schedule rides, coordinate trips, and exchanging information that can benefit our clients as a whole.

Human Resources Center of Edgar and Clark:

- We have a mass transit that provides door to door transportation as needed in a very rural area! The vehicles are accessible and the driver’s are good about helping as appropriate.

LifeLinks:

- It's a small area and I believe that all agencies are supportive of each other and committed to working together to meet the needs of the community.

Macon Resources, Inc.:

- DPTS has been good about communicating any issues with bussing coming or going to our building.

Piattran:

- The strength in Piatt County is working together between the many Piatt County agencies.  There is a new group in the area that is being ran by United Way where all the providers get together monthly and discuss any barriers that they know individuals have specifically.  

Showbus:

- With the above caveat, I would say the SHOW BUS area does well not only utilizing service contracts but also strongly supporting each other’s transportation programs.  We meet monthly to quarterly with county based transportation groups to review unmet needs.

RIDES Rural Mass Transit District:

- HSTP meetings help with networking and discussion of common themes, solutions and barriers.

Shelby County Community Services:

- We are in a community where you work together for the betterment of all

#### Question 3: In your assessment, what enhancements are most needed to improve the coordination of public and human service transportation in your service area?

CCAR:

- More vehicles, especially larger ones.

C-CARTS:

-	One call one click centers
-	A greater number of fixed routes or longer commuter routes between counties could make it so more agencies are on Google Transit, leading to better trip planning via app

CRIS Rural Mass Transit District:

- At this time, I believe the DATS council founded in 2010 is very effective.

Charleston Transitional Facility:

- Increased zipline options
- Free services by public providers to other HSTP members
- Maintenance options

Dial-A-Ride:

- A collaborative agreement between public and human services and Dial-A-Ride would enhance the coordination of public and service transportation in our service area.

Human Resources Center of Edgar and Clark:

- Funding to offset financial losses in order to expand hours. I believe supplemental payments to Mass Transit districts could be made to encourage expanding hours. Even if only temporary to determine demand and sustainability.

LifeLinks:

- Increased service hours

Piattran:

- It would be nice if there was maybe a smaller group of transportation providers that are more local to our area that could meet and discuss ways to coordinate our service better.  We have no problem getting connected with and meeting with providers within our County, it is when we get outside of our County there is a little to no coordination amongst providers.  

Showbus:

- The urban/rural split is difficult.  

RIDES Rural Mass Transit District:

- County lines seem to sometimes present barriers to some providers. Moving forward, social distancing and issues related to that on buses will require even more creativity and coordination on the part of transit agencies and human service organizations. Managed Care/Medicaid seems to still be a work in progress on some levels.

#### Question 4: If there are any other issues, concerns, or information relevant to this topic, please feel free to address them in the space provided below.

CCAR:

- Concern for big buck day program consumer or coordinating all day to keep everyone safe (recheck this)

Charleston Transitional Facility:

- CTF has started utilizing the zipline for work trips and is building some programming around zipline schedule. CTF has explored further services with Dial-A-Ride but DAR has little capacity and insurance was a concern.

Dial-A-Ride:

- When collaboration and coordination meetings are held, it seems that we talk more than we actually act upon our ideas.

LifeLinks:

- LifeLinks provides transportation for clients served by the agency only - to and from a day program, medical appointments, primarily. Many of those coming to LifeLinks for services utilize Dial-A-Ride who is around campus multiple times a day. We link our consumers with Dial-A-Ride daily.

Piattran:

- We specifically have a LOT of service contracts, but then we struggle with the balance of the FTA in percentage of Service Contracts vs general public.  We hate to deny rides to service contracts due to the percentage from the FTA, to then not have a ride during that time.  We aren’t denying the public to help our service contracts, we just have a way larger need from our service contracts. Additionally, figuring out who takes precedence over whom becomes a problem.  For example, does our HeadStart contract trump our Mental Health contract.  Until we receive our new buses this will continue to be an issue for us.  

RIDES Rural Mass Transit District:

- More adaptability from Managed Care Organizations. Hearing long wait times for Medicaid  eligible passengers when calling to schedule rides through their MCO’s.  Possible solutions would be to coordinate trips or split legs between multiple providers. Allowing passengers to contact their transportation provider who could then reach out to the MCO.

Shelby County Community Services:

- It would be helpful, from a maintenance standpoint, if there was a qualified repair shop in close proximity. This closest is Springfield.

#### Question 5: Based on your experience, what are the barriers to coordination of transportation services? (Check all that apply).

<rpc-chart url="q5providerchart.csv"
  chart-title="Trip Type"
  type="horizontalBar"
  x-min="0"
      description="Other: Fear of losing funding if the agency considers a transportation services contract and coordinating routes in a rural setting."
  stacked="false"
  aspect-ratio="3"></rpc-chart>


#### Question 6: Are your agency's transportation services coordinated with other transportation providers in your area?

<rpc-chart url="q6providerchart.csv"
  type="pie"
  aspect-ratio="3"></rpc-chart>

  <rpc-chart url="q6providerchart2.csv"
    chart-title="Coordination participation by service type"
    type="horizontalBar"
    x-min="0"
    stacked="false"
      description="Other: CRIS Rural Mass Transit District: We have an agreement with Vermilion County Courthouse to bring persons in for hearings or rehab as requested. Macon Resources, Inc.: We work independent of other transportation agencies."
    aspect-ratio="3"></rpc-chart>


#### Coordination with central dispatching
*4 out of 11 positive responses*

- C-CARTS
- CRIS Rural Mass Transit District
- Dial-A-Ride
- RIDES Rural Mass Transit District

#### Coordination with referral of clients
*8 out of 11 positive responses*

- C-CARTS
- CRIS Rural Mass Transit District
- Dial-A-Ride
- Human Resource Center of Edgar and Clark
- LifeLinks
- Piattran
- RIDES Rural Mass Transit District
- Showbus

#### Coordination with providing service for people with disabilities
*10 out of 11 positive responses*

- CCAR
- C-CARTS
- Charleston Transitional Facility
- CRIS Rural Mass Transit District
- Dial-A-Ride
- Human Resource Center of Edgar and Clark
- LifeLinks
- Piattran
- RIDES Rural Mass Transit District
- Showbus

#### Coordination with driver training
*4 out of 11 positive responses*

- C-CARTS
- Charleston Transitional Facility
- Dial-a-Ride
- Showbus

#### Coordination with emergency back-up
*2 out of 11 positive responses*

- C-CARTS
- CRIS Rural Mass Transit District

#### Coordination with providing transportation services for another agency
*7 out of 11 positive responses*

- CCAR
- CRIS Rural Mass Transit District
- Dial-A-Ride
- LifeLinks
- Piattran
- RIDES Rural Mass Transit District
- Showbus

#### Coordination with other
*2 out of 11 positive responses*

- CRIS Rural Mass Transit District: "We have an agreement with Vermilion County Courthouse to bring persons in for hearings or rehab as requested."
- Macon Resources, Inc.: "We work independent of other transportation agencies."
