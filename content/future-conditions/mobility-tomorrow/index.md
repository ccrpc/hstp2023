---
title: "Mobility Tomorrow"
draft: false
weight: 10
---

Our region’s transportation landscape has changed significantly since the Covid-19 pandemic. Transportation agencies are confronting challenges with hiring and retention of vital operators and staff, navigating new regulations and sources of funding, and experiencing unprecedented levels of stress and threats to our collective health. Yet, demand for affordable and reliable transportation has remained constant. Planning for the future of mobility in Region 8 is more critical than ever. Using data from transit riders across Region 8, this page will outline goals and recommendations for the future. 

## Leveraging Trip Purpose Data to Strengthen Service

We can learn a lot from the reasons people travel the way they do. Some people travel to access recreational opportunities like exercise classes and social events. Others travel to get to work or important medical appointments. Whatever the purpose, reliable and affordable transportation service is vital to the wellbeing of individuals and communities. 

  <rpc-chart url="trip_purpose.csv"
    chart-title="Trip Purpose for Region 8 Survey Respondents (percent)"
    type="pie"
    description="Blank responses not included in the above chart."
    caption="Blank responses not included in chart."
    aspect-ratio="3"></rpc-chart>

Many riders - 53% of C-CARTS riders and 33% of Piattran Riders - use the rural transit service to get to work. However, riders' needs differ by region. Among Coles County Dial-a-Ride survey respondents, only 3% stated they use the service to get to work. 32% of C-CARTS riders, 46% of Coles County Dial-a-Ride, and 24% of Piattran riders use the service to get to medical-related appointments. 5% of C-CARTS riders and 14% of Piattran riders use the service to get to shopping; Coles County Dial-a-Ride does not track this trip type. 11% of C-CARTS riders, 31% of Coles County Dial-a-Ride riders, and 29% of Piattran riders use rural public transit service for other trip purposes. In the past, C-CARTS surveys have included ‘social’ as a trip type. This category accounted for trips related to recreational activities, meetings with friends or family, and other social appointments. Since Covid-19, C-CARTS has stopped collecting data on this type of trip type as social trips virtually stopped. However, future survey efforts should reincorporate this trip type into answer options. Overall, C-CARTS primarily serves riders going to work and to medical-related appointments. Piattran and Coles County Dial-a-Ride included social trips in its survey data. In 2022, among Piattran survey respondents, 5% used the service for social-related trips and, according to Coles County Dial-a-Ride survey respondents, 13% used the service for social-related trips.

Given this trip purpose data, agencies should focus on ways to better accommodate riders commuting to and from work, and those with chronic medical conditions. Trip denials are an unfortunate but inevitable result of having limited transportation resources. For logistical reasons, most agencies will have to deny a trip request from time to time. However, agencies can work to limit trip denials for medical purposes by tracking the trip purpose of denied trips. Once such data has been collected by the HSTP coordinator, the group and subcommittees should collaborate on ways to minimize trip denials for important medical trips.

Another aspect of accommodating medical trips is reinforcing communication with healthcare providers and other parties involved in riders’ medical appointments. The consequences of riders missing their pick-up or drop-off times can sometimes be expensive and life-threatening. Agencies and healthcare providers must coordinate patient drop-off and pick-up times, plans for possible delays during appointments or procedures, and other considerations specific to the rider’s situation.

### Recommendations

- Begin tracking trip purposes among trip denials.
- Look for trends in medical trip denials.
- Establish strong relationships between transit agencies and healthcare providers in each service area.
- Coordinate pick-up and drop-off locations and times with healthcare providers before medical trips take place.
- Streamline surveys across Region 8.

### Goal

- Reduce medical trip denials by 10% before 2025.
- Establish clear lines of communication between rural transit providers and helathcare agences in each county of Region 8. Use HSTP Region 8 meetings as a venue to foster these relationships.

## Centering Transit-Dependent Riders

Transit dependence is the state of being reliant on transit for your mobility needs, and it is a diverse category. People who are transit dependent could fit into this category for several reasons. The category includes people who are unable to drive due to a medication or medical condition, people under the age of 16, people who have a physical or cognitive disability that prevents them from driving, and people who have lost their driver's license due to a conviction or other reason. Some people are transit-dependent by choice, meaning they may not like driving due to its environmental impacts, safety risks, or driving anxiety due to trauma or another cause. The existence of transit-dependent groups is a key reason why public transit is so important. 

  <rpc-chart url="transit_dependence.csv"
    chart-title="Transit Dependence Among Region 8 Survey Respondents (percent)"
    type="pie"
    description="Blank responses not included in the above chart."
    caption="Blank responses not included in chart."
    aspect-ratio="3"></rpc-chart>

42% of C-CARTS riders, 29% of Coles County Dial-a-Ride riders, and 42% of Piattran riders are dependent on the service for the trips they take. In other words, an average of 38% of Region 8 riders wouldn’t be able to make the trip without rural public transit service. Another 32% of C-CARTS riders and 7% of Coles County Dial-a-Ride riders would be able to get a ride from someone else to replace the trip, 11% of C-CARTS riders and 3% of Coles County Dial-a-Ride riders would be able to drive themselves, and 5% of C-CARTS riders and 7% of Coles County Dial-a-Ride riders would either walk, bike, or use a mobility device to make the trip, and 11% from both agencies would have another arrangement in place. Sustaining and expanding service is the best way to cater to transit-dependent riders. 

To learn more about how to better serve this group, agencies should gather more data on them. Transit agencies in Region 8 have different ways of wording survey questions that probe for transit-dependence and access to other modes of transportation. To obtain more accurate and insightful information on transit-dependence in Region 8, survey questions should be streamlined across the region.

### Recommendations

- Deploy survey designed for transit-dependent riders.
- Streamline surveying efforts across the region by establishing at least 1-3 region-wide survey questions for transportation providers.

### Goals

- Collaborate on a region-wide survey for transit providers *and* other social services agencies in Region 8.
- Display streamlined survey data in an online data dashboard.

## Breaking Down Barriers to Mobility

Mobility isn't a problem for most able-bodied adults with access to a car. But many people with disabilities, people without access to a car or driver's license, children and older adults face obstacles to mobility everyday. It is the job of transit and paratransit agencies to break down those obstacles and ensure that everyone has access.

About half, or 48%, of C-CARTS riders indicated they face no barriers to mobility. 20% said the system hours are a barrier, 16% said their personal health is a barrier, 11% said advance reservation requirements are a barrier, and 5% said a barrier is that they can’t find someone to ride with. Overall, 95% of C-CARTS riders indicated they’d benefit from increased C-CARTS service.

  <rpc-chart url="obstacles.csv"
    chart-title="Obstacles to Mobility Among Region 8 Survey Respondents (percent)"
    type="pie"
    description="Blank responses not included in the above chart."
    caption="Blank responses not included in chart."
    aspect-ratio="3"></rpc-chart>

With a fifth of riders indicating that limited system hours are an obstacle to their mobility, agencies must place a strong focus on filling operator and staff vacancies so they are in a position to sustain and expand service. To do this, agencies must place a strong focus on being exemplary employers that compare favorably to other popular employers in their locale. 

To address the fact that advance reservation requirements are a barrier for some riders, we must gather more data on the current modes of dispatching and scheduling on-demand trips. In the near future, the HSTP coordinator will compile this data and collaborate with the group to streamline and shorten on-demand trip reservation processes across Region 8.

### Recommendations

- Address operator hiring and retention challenges by raising wages and adding benefits where possible.
- Ensure wage increases and other employee benefits are financially sustainable into the future, even after Covid-19-related funds are used.
- Gather data on current dispatching and scheduling methods (software, advance scheduling requirements, and other rules).

### Goal

- Regularly share information related to hiring and retention issues and wage changes at HSTP Region 8 meetings.

## Serving Riders with Disabilities

About 40% of C-CARTS riders surveyed have a disability. See video to watch Sheila Greuel’s ‘Transporting People Experiencing Cognitive Decline’ presentation to the HSTP Region 8 committee. Even though a large portion of riders have a disability, not all agencies require their operators to go through training related to this issue. Specifically, Region 8 members have called attention to the importance of safely transporting people experiencing cognitive decline. Operators already have a lot on their plates, but once hiring and retention stabilizes, agencies should consider training operators on this issue. Agencies can partner with local mental health and disability advocacy groups to design training tailored to their specific location.

  <rpc-chart url="disability.csv"
    chart-title="Disability Status Among Region 8 Survey Respondents (percent)"
    type="pie"
    description="Blank responses not included in the above chart."
    caption="Blank responses not included in chart."
    aspect-ratio="3"></rpc-chart>

### Recommendations

- Invest in resources, such as training and workshops, to educate operators and the broader community on transporting riders with disabilities, including riders experiencing cognitive decline.
- Evaluate the feasibility of requiring operators to train in the transportation of riders with disabilities.
- Expand access to travel training for people with disabilities.

### Goal

- Regularly share outreach and travel training best practices at HSTP Region 8 meetings.